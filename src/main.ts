import { helloOpt } from './option';
import { helloDefault } from './default';
import { sum } from './rest';
import { func1 } from './callback';
import { func2 } from './callback';
import { enclosure } from './closure';

(() => {
  console.log(helloOpt()); // hello!
  console.log(helloOpt('option')); // hello option!

  console.log(helloDefault()); // hello world!
  console.log(helloDefault('default')); // hello default!

  console.log(sum(1)); // 1
  console.log(sum(7, 8, 9)); // 24
  const array = [1, 2, 3, 4, 5];
  console.log(sum(...array)); // 15
  const tuple: readonly [number, number] = [25, 75];
  console.log(sum(...tuple)); // 100

  func1(() => console.log('hello callback!')); // hello callback!
  func2(console.log); // func2

  const count = enclosure();
  console.log(count()); // 1
  console.log(count()); // 2
  console.log(count()); // 3

  // const x = Math.floor(Math.random() * Math.floor(2)); // 0 or 1
  // const result = x === 1 ? 'hit!!' : 'miss...';
  // 宗教的な理由で上記の様な三項演算子が使えない人のための即時実行関数
  const result = (() => {
    if (Math.floor(Math.random() * Math.floor(2))) {
      return 'hit!!';
    }
    return 'miss...';
  })();
  console.log(result); // hit!! or miss...
})();
