/**
 * 残余引数が定義された関数
 *
 * @param { ...number[] } nums 単一や複数の number の値を持つ配列やタプル等のオブジェクト
 * @returns { number } 引数の要素の合計値
 */
export const sum = (...nums: number[]): number => {
  return nums.reduce((prev, crnt) => prev + crnt);
};
