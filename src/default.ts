/**
 * デフォルト引数が定義された関数
 *
 * @param { string } 渡されなかった場合や undefined が渡された場合 'world' となる
 * @returns { string } `hello ${defarg}!`
 */
export const helloDefault = (defarg = 'world'): string => {
  return `hello ${defarg}!`;
};
